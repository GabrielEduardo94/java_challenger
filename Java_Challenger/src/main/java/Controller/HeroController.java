package Controller;

import java.util.*;

import javax.validation.Valid;

import Model.Hero;
import Model.Power;
import Model.Universe;
import Repository.HeroRepository;
import Repository.PowerRepository;
import Repository.UniverseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class HeroController
{
    @Autowired
    private HeroRepository heroRepository;
    @Autowired
    private PowerRepository powerRepository;
    @Autowired
    private UniverseRepository universeRepository;

    @GetMapping("/herois")
    public List<Hero> getAllHeros()
    {
        return heroRepository.findAll();
    }

    @GetMapping("/poderes")
    public List<Power> getAllPowers()
    {
        return powerRepository.findAll();
    }

    @GetMapping("/universos")
    public List<Universe> getAllUniverses()
    {
        return universeRepository.findAll();
    }

    @GetMapping("/herois/{id}")
    public ResponseEntity<Optional<Hero>> getHerosById(@PathVariable(value = "id") Long heroId)
    {
        Optional<Hero> heroi = heroRepository.findById(heroId);
        return ResponseEntity.ok().body(heroi);
    }

    @PostMapping("/herois")
    public Hero createHero(@Valid @RequestBody Hero heroi)
    {
        heroi.setDataCadastro(new Date());

        return heroRepository.save(heroi);
    }

    @PutMapping("/herois/{id}")
    public ResponseEntity<Hero> updateHero
            (
            @PathVariable(value = "id") Long heroId,
            @Valid
            @RequestBody Hero heroDetails
            )
    {
        Optional<Hero> heroi = heroRepository.findById(heroId);

        if (heroi.isPresent())
        {
            heroi.get().setId(heroDetails.getId());
            heroi.get().setNome(heroDetails.getNome());
            heroi.get().setAtivo(heroDetails.isAtivo());
            heroi.get().setUniverso(heroDetails.getUniverso());
            heroi.get().setPoderes(heroDetails.getPoderes());
            heroi.get().setDataCadastro(heroDetails.getDataCadastro());
        }
        final Hero updatedHero = heroRepository.save(heroi.get());

        return ResponseEntity.ok(updatedHero);
    }
}

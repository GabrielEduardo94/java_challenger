package Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "tbHero")
public class Hero {

    private long id;
    private String Nome;
    private Universe Universo;
    private Date DataCadastro;
    private boolean Ativo;



    @ManyToMany
    @JoinTable(name="tbHeroPower", joinColumns=
            {@JoinColumn(name="Hero_id")}, inverseJoinColumns=
            {@JoinColumn(name="Power_id")})
    private List Poderes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "Nome", nullable = false)
    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    @Column(name = "Universo")
    public Universe getUniverso() {
        return Universo;
    }

    public void setUniverso(Universe universo) {
        Universo = universo;
    }
    @Column(name = "DataCadastro")
    public Date getDataCadastro() {
        return DataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        DataCadastro = dataCadastro;
    }

    @Column(name = "Ativo")
    public boolean isAtivo() {
        return Ativo;
    }

    public void setAtivo(boolean ativo) {
        Ativo = ativo;
    }

    public List getPoderes() {
        return Poderes;
    }

    public void setPoderes(List poderes) {
        Poderes = poderes;
    }


    @Override
    public String toString() {
        return "Hero{" +
                "id=" + id +
                ", Nome='" + Nome + '\'' +
                ", Poderes=" + Poderes +
                ", Universo=" + Universo +
                ", DataCadastro=" + DataCadastro +
                ", Ativo=" + Ativo +
                '}';
    }
}

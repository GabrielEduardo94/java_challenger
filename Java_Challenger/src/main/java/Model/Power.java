package Model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tbPower")
public class Power {

    private long Id;
    private String Nome;

    @ManyToMany(mappedBy = "poderes")
    private List Herois;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    @Column(name = "Nome")
    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    @Override
    public String toString() {
        return "Power{" +
                "Id=" + Id +
                ", Nome='" + Nome + '\'' +
                '}';
    }
}

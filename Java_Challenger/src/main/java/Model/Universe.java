package Model;

import javax.persistence.*;

@Entity
@Table(name = "tbUniverse")
public class Universe {

    private long Id;
    private String Nome;

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    @Column(name = "Nome")
    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    @Override
    public String toString() {
        return "Power{" +
                "Id=" + Id +
                ", Nome='" + Nome + '\'' +
                '}';
    }
}

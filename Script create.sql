SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_hero](
	[hero_id] [bigint] NOT NULL,
	[ativo] [bit] NULL,
	[data_cadastro] [datetime2](7) NULL,
	[nome] [varchar](255) NOT NULL,
	[id_universe] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tb_hero] ADD PRIMARY KEY CLUSTERED 
(
	[hero_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tb_hero]  WITH CHECK ADD  CONSTRAINT [FKerrpbjv16a63dvpddn8lt91s2] FOREIGN KEY([id_universe])
REFERENCES [dbo].[tb_universe] ([id_universe])
GO
ALTER TABLE [dbo].[tb_hero] CHECK CONSTRAINT [FKerrpbjv16a63dvpddn8lt91s2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_power](
	[id_power] [bigint] IDENTITY(1,1) NOT NULL,
	[nome] [varchar](255) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tb_power] ADD PRIMARY KEY CLUSTERED 
(
	[id_power] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_universe](
	[id_universe] [bigint] IDENTITY(1,1) NOT NULL,
	[nome] [varchar](255) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tb_universe] ADD PRIMARY KEY CLUSTERED 
(
	[id_universe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_hero_poderes](
	[hero_hero_id] [bigint] NOT NULL,
	[poderes_id_power] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tb_hero_poderes]  WITH CHECK ADD  CONSTRAINT [FKckcemjfw0go5cbr5rd5foc5tv] FOREIGN KEY([hero_hero_id])
REFERENCES [dbo].[tb_hero] ([hero_id])
GO
ALTER TABLE [dbo].[tb_hero_poderes] CHECK CONSTRAINT [FKckcemjfw0go5cbr5rd5foc5tv]
GO
ALTER TABLE [dbo].[tb_hero_poderes]  WITH CHECK ADD  CONSTRAINT [FKi9r2lkj13jk2q9q5olpkjxb24] FOREIGN KEY([poderes_id_power])
REFERENCES [dbo].[tb_power] ([id_power])
GO
ALTER TABLE [dbo].[tb_hero_poderes] CHECK CONSTRAINT [FKi9r2lkj13jk2q9q5olpkjxb24]
GO
